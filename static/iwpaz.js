// IWPAZ: Interface Web Pour Accéder à Zaulta
// A Zaulta web interface
// Copyright (C) 2017 Whidou <whidou@openmailbox.org>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.

var $clear = $("#clear");
var $content = $("#content");
var $result = $("#result");
var $rows = $(".element");
var $searchfield = $("#searchfield");

function updateList() {
    var val = $.trim($searchfield.val());
    if (val === "") {
        $rows.show();
    } else {
        $rows.hide();
        $rows.filter(":contains('" + val + "')").show();
    }
}

function loadResult(table) {
    var url = $(location).attr('href').split("#")[0] +
               encodeURIComponent(table) +
               "?random=" + Math.random();
    $content.html('<textarea id="result"></textarea>');
    $content.children().first().load(url).hide().fadeIn();
    $result = $("#result");
}

// Disable AJAX caching
$.ajaxSetup ({
    cache: false
});

// Search form
$("#search").submit(function() {
    loadResult($.trim($searchfield.val()));
});

// Search input
$searchfield.keyup(updateList);
$searchfield.focus();

// Roll
$(".element").click(function() {
    loadResult($(this).find(".tablename").text());
});

// Clear
$clear.click(function() {
    $searchfield.val('');
    updateList();
    $searchfield.focus();
});
$clear.dblclick(function() {
    $result.val('');
});

// Copy
$("#copy").click(function() {
    $result.focus();
    $result[0].setSelectionRange(0, $result.val().length);
    document.execCommand("copy");
    $searchfield.focus();
});
