#!/usr/bin/python3

# IWPAZ: Interface Web Pour Accéder à Zaulta
# A Zaulta web interface
# Copyright (C) 2017 Whidou <whidou@openmailbox.org>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import flask
import zaulta
import re
import os.path


app = flask.Flask(__name__)
generator = {}
tables_path = os.path.join(app.root_path, "tables")


@app.route("/")
def root():
    return flask.render_template("index.htm")


@app.route("/<filename>/")
def picker(filename):
    if not re.match(r"^[a-z0-9]+$", filename):
        return ""
    if filename not in generator:
        path = os.path.join(tables_path, "{}.json".format(filename))
        generator[filename] = zaulta.Tables(path)
    tables = generator[filename][:]
    tables.sort()
    return flask.render_template("iwpaz.htm", tables=tables)


@app.route("/<filename>/<table>")
def result(filename, table):
    if filename not in generator:
        return ""
    if table not in generator[filename][:]:
        return ""
    return generator[filename][table]
